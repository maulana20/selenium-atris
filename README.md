# selenium-atris

## Getting Started

Proses yang di jalankan :
- jalankan pyramid untuk memanggil proses http://localhost:6543/atris?username=xxx&password=xxx
- tampil dialog popup proses login hingga logout
- screen log pada tiap proses dengan format png
- download web driver google : chromedrive.exe, firefox : geckodriver.exe

python yang di gunakan :
```bash
Python 3.7.0
```

jalankan perintah :
```bash
pserve development.ini --reload
```

pip yang di butuhkan :
```bash
pip install selenium==3.141.0
```

Tampilan

![demo](demo.png)
