import re
import datetime
import iso8601

from bs4 import BeautifulSoup

import logging
log = logging.getLogger(__name__)

class RequestModel(object):
	def __init__(self, request):
		self.request = request
		# declare variable
		self.ISODATE = 'ISODATE'
		self.ISODATETIME = 'ISODATETIME'
		self.TIME = 'TIME'
		self.ALPHANUMERIC = 'ALPHANUMERIC'
		self.ALPHANUMERIC_SPACE = 'ALPHANUMERIC_SPACE'
		self.NUMERIC = 'NUMERIC'
		self.ALPHABET = 'ALPHABET'
		self.ALPHABET_SPACE = 'ALPHABET_SPACE'
		self.EMAIL = 'EMAIL'
		self.TEXT = 'TEXT'
		self.BOOLEAN = 'BOOLEAN'
		self.INITIAL_CODE = 'INITIAL_CODE'
	
	def isIsoDate(self, _input):
		return datetime.datetime.strptime(_input, '%Y-%m-%d')
	
	def isIsoDatetime(self, _input):
		datetime.datetime.strptime(_input, '%Y-%m-%dT%H:%M:%S.%f')
		return iso8601.parse_date(_input)
	
	def isTime(self, _input):
		return datetime.datetime.strptime(_input, '%H:%M')
	
	def isAlphaNumeric(self, _input):
		return re.compile(r"^\w+$").match(_input)
	
	def isAlphaNumericWithSpace(self, _input):
		return re.compile(r"^[A-Za-z0-9\s]+$").match(_input)
		
	def isNumeric(self, _input):
		return re.compile(r"^[-+]?[0-9]+$").match(_input)
	
	def isAlphabet(self, _input):
		return re.compile(r"^[a-zA-Z]+$").match(_input)
	
	def isAlphabetWithSpace(self, _input):
		return re.compile(r"^[a-zA-Z\s]+$").match(_input)

	def isEmail(self, _input):
		return re.compile(r"[^@]+@[^@]+\.[^@]+").match(_input)
	
	def isText(self, _input):
		return (re.compile(r"^[A-Za-z0-9\/\s\t\n.,:;?!'\()%\"\-\+@]+$").match(_input))

	def isBoolean(self, _input):
		return (re.compile(r"^[0-1]+$").match(_input))
	
	def isInitialCode(self, _input):
		return (re.compile(r"^[A-Z]+$").match(_input))
	
	def isComplete(self, param_req):
		log.info(str(self.request.params))
		
		reason = []
		
		for item in param_req:
			if item[0] not in self.request.params:
				reason.append(item[0] + ' not found !')
				continue
			
			if item[1] == self.ISODATE:
				valid = self.isIsoDate(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ISODATE)
			elif item[1] == self.ISODATETIME:
				valid = self.isIsoDatetime(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ISODATETIME)
			elif item[1] == self.TIME:
				valid = self.isTime(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.TIME)
			elif item[1] == self.ALPHANUMERIC:
				valid = self.isAlphaNumeric(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ALPHANUMERIC)
			elif item[1] == self.ALPHANUMERIC_SPACE:
				valid = self.isAlphaNumericWithSpace(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ALPHANUMERIC_SPACE)
			elif item[1] == self.NUMERIC:
				valid = self.isNumeric(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.NUMERIC)
			elif item[1] == self.ALPHABET:
				valid = self.isAlphabet(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ALPHABET)
			elif item[1] == self.ALPHABET_SPACE:
				valid = self.isAlphabetWithSpace(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.ALPHABET_SPACE)
			elif item[1] == self.EMAIL:
				valid = self.isEmail(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.EMAIL)
			elif item[1] == self.TEXT:
				valid = self.isText(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.TEXT)
			elif item[1] == self.BOOLEAN:
				valid = self.isBoolean(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.BOOLEAN)
			elif item[1] == self.INITIAL_CODE:
				valid = self.isInitialCode(self.request.params[item[0]])
				if not valid:
					reason.append(item[0] + ' must ' + self.INITIAL_CODE)
		
		if len(reason) > 0:
			log.info(' '.join(reason))
			return False
		
		return True

class AtrisModel(RequestModel):
	def __init__(self, request):
		RequestModel.__init__(self, request)
		
		self.param_req = ( ('username', self.ALPHANUMERIC), ('password', self.ALPHANUMERIC) )
	
	def is_onlogin(self, source):
		bs = BeautifulSoup(source, 'html.parser')
		body = bs.find('div', style="font-size: 12pt;")
		body = str(body)
		body = body[(body.find(">") + 1):]
		matches = body[:body.find(",")]
		matches = ''.join(matches.split("\n")).strip()
		matches = matches.lower()
		
		if matches == 'anda sudah login pada device lain':
			return True
		
		return False
