from pyramid.response import Response
from pyramid.view import view_config

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as selopt

from .models import AtrisModel

import logging
log = logging.getLogger(__name__)

class SeleniumView(object):
	
	def __init__(self, request):
		self.request = request
		self.web_driver = None
	
	@view_config(route_name='home')
	def home(self):
		return Response('<body>Visit <a href="/howdy">hello</a></body>')
	
	@view_config(route_name='atris')
	def atris(self):
		request = self.request
		
		atris = AtrisModel(self.request)
		is_complete = atris.isComplete(atris.param_req)
		
		if not is_complete:
			return Response('Not Complete')
		
		username = request.params['username']
		password = request.params['password']
		
		self.web_driver = webdriver.Chrome(chrome_options=selopt().set_headless(headless=True), executable_path='chromedriver.exe')
		self.web_driver.get('http://atris.versatech.co.id')
		
		sleep(1)
		self.web_driver.save_screenshot("ss/login.png")
		log.info('atris login')
		
		self.web_driver.find_element_by_xpath("//input[@placeholder='Username']").send_keys(username)
		self.web_driver.find_element_by_xpath("//input[@placeholder='Password']").send_keys(password)
		self.web_driver.find_element_by_xpath("//button[contains(text(), 'Login')]").click()
		sleep(2)
		
		is_onlogin = atris.is_onlogin(self.web_driver.page_source)
		if is_onlogin == True:
			self.web_driver.save_screenshot("ss/is_onlogin.png")
			log.info('atris is onlogin')
			self.web_driver.find_element_by_xpath("//label[contains(text(), 'Setuju')]").click()
			self.web_driver.find_element_by_xpath("//button[@class='btn btn-primary']").click()
			sleep(2)
		
		element_beranda = self.web_driver.find_element_by_xpath("//span[@class='menu-item-parent']")
		attempt = 0
		while not element_beranda.is_displayed():
			log.info('atris masih menunggu tampil menu')
			attempt += 1
			sleep(1)
			element_beranda = self.web_driver.find_element_by_xpath("//span[@class='menu-item-parent']")
		
		if attempt > 5:
			raise Exception('atris menunggu tampil menu sudah 5 detik')
		
		self.web_driver.save_screenshot("ss/home.png")
		
		self.web_driver.find_element_by_xpath("//span[contains(text(), 'Administrasi')]").click()
		sleep(1)
		self.web_driver.execute_script("$('li.open ul li a')[4].click()")
		sleep(2)
		
		self.web_driver.find_elements_by_id('logout')[0].click()
		self.web_driver.save_screenshot("ss/logout.png")
		log.info('atris logout')
		
		self.web_driver.quit()
		
		return Response('Done')
