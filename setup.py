from setuptools import setup

requires = [
	'pyramid',
	'waitress',
]

dev_requires = [
	'pyramid_debugtoolbar',
]

setup(
	name = 'selenium_atris',
	install_requires = requires,
	extra_require = {
		'dev': dev_requires,
	},
	entry_points = {
		'paste.app_factory': [
			'main = selenium_atris:main'
		],
	},
)
